import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

public class MainTest {

    public static void login( WebDriver driver) throws InterruptedException {

        WebElement element = driver.findElement(By.className("desktop-userTitle"));
        element.click();

        WebElement login = driver.findElement(By.className("desktop-linkButton"));
        login.click();

        WebElement mobileNum = driver.findElement(By.className("mobileNumberInput"));
        mobileNum.sendKeys("9494021995");
        mobileNum.click();


        WebElement submit = driver.findElement(By.className("submitBottomOption"));
        submit.click();
        Thread.sleep(30000);
        submit.click();

        WebElement password = driver.findElement(By.name("Log in using "));
        password.click();
    }

    public static void selectingCategory(WebDriver myntra) throws InterruptedException {


        WebElement men = myntra.findElement(By.xpath("//*[@id=\"desktop-header-cnt\"]/div[2]/nav/div/div[1]/div/a"));
        WebElement women = myntra.findElement(By.xpath("//*[@id=\"desktop-header-cnt\"]/div[2]/nav/div/div[2]/div/a"));
        WebElement kids = myntra.findElement(By.xpath("//*[@id=\"desktop-header-cnt\"]/div[2]/nav/div/div[3]/div/a"));
        WebElement homeAndLeaving = myntra.findElement(By.xpath("//*[@id=\"desktop-header-cnt\"]/div[2]/nav/div/div[4]/div/a"));
        WebElement Beauty = myntra.findElement(By.xpath("//*[@id=\"desktop-header-cnt\"]/div[2]/nav/div/div[5]/div/a"));
        WebElement Studio = myntra.findElement(By.xpath("//*[@id=\"desktop-header-cnt\"]/div[2]/nav/div/div[6]/div/a"));
//        WebElement women = myntra.findElement(By.xpath("//*[@id=\"desktop-header-cnt\"]/div[2]/nav/div/div[2]/div/a"))''

        Actions act = new Actions(myntra);
        act.moveToElement(men).perform();
        Thread.sleep(1000);
        act.moveToElement(women).perform();
        Thread.sleep(1000);
        act.moveToElement(kids).perform();
        Thread.sleep(1000);
        act.moveToElement(homeAndLeaving).perform();
        Thread.sleep(1000);
        act.moveToElement(Beauty).perform();
        Thread.sleep(1000);
        act.moveToElement(Studio).perform();
        Thread.sleep(1000);
        act.moveToElement(men).perform();

    }

    public static void Scroll(WebDriver myntra) throws InterruptedException {
        Actions act = new Actions(myntra);
        act.sendKeys(Keys.PAGE_DOWN).build().perform();
        Thread.sleep(2000);
        act.sendKeys(Keys.PAGE_UP).build().perform();
    }

    public static void Search(WebDriver myntra) throws InterruptedException {
        WebElement Product = myntra.findElement(By.xpath("//*[@id=\"desktop-header-cnt\"]/div[2]/div[3]/input"));
        Product.sendKeys("T Shirts");
        Thread.sleep(1000);
        WebElement search = myntra.findElement(By.className("desktop-submit"));
        search.click();
    }

    public static void SelectingProduct(WebDriver myntra){
        myntra.findElement(By.className("product-imageSliderContainer")).click();
        ArrayList<String> AllTabs = new ArrayList<String>(myntra.getWindowHandles());
        myntra.switchTo().window(AllTabs.get(1));
        JavascriptExecutor js = (JavascriptExecutor) myntra;
        myntra.findElement(By.xpath("//*[@id=\"sizeButtonsContainer\"]/div[2]/div[3]/div[1]/button")).click();
        myntra.findElement(By.xpath("//*[@id=\"mountRoot\"]/div/div/div/main/div[2]/div[2]/div[3]/div/div[1]")).click();
    }

    public static void main(String[] args) throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        WebDriver myntra = new ChromeDriver();

        myntra.get("https://www.myntra.com/");
        myntra.manage().window().maximize();
        Thread.sleep(1000);
        Scroll(myntra);
//        login(myntra);
        selectingCategory(myntra);
        Search(myntra);
        SelectingProduct(myntra);
    }
}
